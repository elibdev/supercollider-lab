~src.freeAll;
s.record;
s.stopRecording;
s.plotTree;
(
~notes = Array.newClear(128);
~bend = 8192;
~mod = 0;
SynthDef(\star, {
	arg freq = 440,
	out=0,
	gate = 0,
	vel = 0.5,
	waverA = 1,
	waverF = 0.01,
	attack = 1,
	release = 3,
	ovrtn = 0.6;
	var sig = 0, env, par;
	par = (1, 3..20);
	//aliasing prevention
	par = (par * freq).minNyquist;
	par.do {
		arg par;
		//modulate freq w 2 LFOs
		var lfo = SinOsc.kr(Rand(0.1, 0.7), Rand(0, 1));
		lfo = lfo * SinOsc.kr(Rand(0.1, 0.7), Rand(0, 1));
		lfo = (lfo * waverF * par) + par;
		//add partial to existing signal
		sig = sig + (SinOsc.ar(lfo, Rand(0, 1)) * (1 / pow(par / freq, ovrtn.reciprocal)));
		//modulate amp w 2 LFOs
		sig = sig * SinOsc.kr(
			Rand(10, 40) * waverA,
			Rand(0, 1),
			waverA.expexp(0.0002, 0.5, 0.05, 0.2),
			1
		).range;
		sig = sig * SinOsc.kr(
			Rand(4, 9),
			Rand(0, 1),
			0.05,
			1
		).range;
	};
	env = EnvGen.kr(Env([0, 1, 0], [attack, release], [\sine, -2], 1), gate, doneAction:2);
	sig = sig * env * vel * ovrtn.linlin(0.2, 2, 1, 1.5).reciprocal;
	Out.ar(out, sig!2);
}).add;

MIDIdef.noteOn(\attack, {
    arg vel, nn, chan, src;
	var weight;
	//weight notes so the high notes don't overpower the low ones
	weight = nn.linlin(0, 127, 1, 8).reciprocal;
	~notes[nn] = Synth(
		\star,
		[
			\freq, nn.midicps,
			\vel, vel.linexp(0, 127, 0.001, 0.3) * weight,
			\gate, 1,
			\out, ~clip2,
			\waverA, ~bend.linexp(0, 16383, 0.0002, 0.5),
			\attack, 0.3,
			\release, 3,
			\ovrtn, ~mod.linexp(0, 127, 0.2, 2),
	], ~src
	);
});

MIDIdef.noteOff(\release, {
	arg vel, nn;
	~notes[nn].set(\gate, 0);
	~notes[nn] = nil;
});

MIDIdef.bend(\bend, {
	arg val, chan, src;
	~bend = val;
	val.linexp(0, 16383, 0.0002, 0.5).postln;
	~notes.do{arg synth;
		{
 			if ((synth != nil),
				synth.set(\waverA, val.linexp(0, 16383, 0.0002, 0.5)));
		}.value;
	};
});

MIDIdef.cc(\control, {
	arg val;
	~mod = val;
	~notes.do{arg synth;
		{
			if ((synth != nil),
				synth.set(\ovrtn, val.linexp(0, 127, 0.2, 2)))
		}.value;
	}
}, 1);
)
