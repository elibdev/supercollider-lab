(
~depth = Group(~src);

SynthDef(\depth1, {
	arg vel = 0.3,
	freq = 50,
	out = ~fx;
	var sig, envF, envA;
	envF = EnvGen.kr(Env([1, 0], [0.2], 2));
	envA = EnvGen.kr(Env.perc(0.01, 0.2, 1, -2));
	//when it's hit hard, the pitch is a little higher
	freq = envF.exprange(freq/3, freq) * vel.linlin(1, 127, 1, 1.1);
	sig = Pulse.ar(freq, 0.5, vel)!2;
	sig = sig * envA;
	sig = BLowPass4.ar(sig, freq*3) * 2.5;
	Out.ar(out, sig);
	//without delaying the freeing of the synth, it pops at end
	FreeSelf.kr(TDelay.kr(Done.kr(envA), 0.01));
}).add;

SynthDef(\depth2, {
	arg vel = 0.3,
	freq = 50,
	out = ~fx;
	var sig, envF, envA;
	envF = EnvGen.kr(Env([0, 1], [0.2], 2));
	envA = EnvGen.kr(Env.perc(0.01, 0.2, 1, -2));
	//when it's hit hard, the pitch is a little higher
	freq = envF.exprange(freq/3, freq) * vel.linlin(1, 127, 1, 1.1);
	sig = Pulse.ar(freq, 0.5, vel)!2;
	sig = sig * envA;
	sig = BLowPass4.ar(sig, freq*3) * 2.5;
	Out.ar(out, sig);
	//without delaying the freeing of the synth, it pops at end
	FreeSelf.kr(TDelay.kr(Done.kr(envA), 0.01));
}).add;

SynthDef(\depth3, {
	arg vel = 0.3,
	freq = 50,
	out = ~fx;
	var sig, envF, envA;
	envF = EnvGen.kr(Env([1, 0, 1], [0.1, 0.1], [-2, 2]));
	envA = EnvGen.kr(Env.perc(0.01, 0.3, 1, -2));
	//when it's hit hard, the pitch is a little higher
	freq = envF.exprange(freq/3, freq) * vel.linlin(1, 127, 1, 1.1);
	sig = Pulse.ar(freq, 0.5, vel)!2;
	sig = sig * envA;
	sig = BLowPass4.ar(sig, freq*3) * 2.5;
	Out.ar(out, sig);
	//without delaying the freeing of the synth, it pops at end
	FreeSelf.kr(TDelay.kr(Done.kr(envA), 0.01));
}).add;

SynthDef(\depth4, {
	arg vel = 0.3,
	freq = 50,
	out = ~fx;
	var sig, envF, envA;
	envF = EnvGen.kr(Env([0, 1, 0], [0.1, 0.1], [-2, 2]));
	envA = EnvGen.kr(Env.perc(0.01, 0.3, 1, -2));
	//when it's hit hard, the pitch is a little higher
	freq = envF.exprange(freq/3, freq) * vel.linlin(1, 127, 1, 1.1);
	sig = Pulse.ar(freq, 0.5, vel)!2;
	sig = sig * envA;
	sig = BLowPass4.ar(sig, freq*3) * 2.5;
	Out.ar(out, sig);
	//without delaying the freeing of the synth, it pops at end
	FreeSelf.kr(TDelay.kr(Done.kr(envA), 0.01));
}).add;


MIDIdef.noteOn(\hit, {
    arg vel, nn;
	var freq, shp;
	freq = switch(floor(nn / 4.0),
		//4th row
		0.0, {45},
		//3rd row
		1.0, {60},
		//2nd row
		2.0, {80},
		//1st row
		3.0, {120},
	);
	shp = switch(nn % 4,
		//4th column
		0, {\depth1},
		//3rd column
		1, {\depth2},
		//2nd column
		2, {\depth3},
		//1st column
		3, {\depth4},
	);
	Synth(shp,
		[
			\vel, vel.linlin(1,127,0.01,0.3),
			\freq, freq,
	], ~depth);
});
)