# SuperCollider Lab

This is a collection of experiments using SuperCollider to build polyphonic MIDI synthesizers.
They have parameters that will change the sound based on control inputs,
such as the velocity of a key press event, or the position of a modulation wheel.

You need to install [SuperCollider](https://supercollider.github.io/) to run these. 

## Rhythm Synths

- [click](click.scd)
- [depth bass](depth-bass.scd)
- [sand](sand.scd)

## Tonal Synths

- [moon](moon.scd)
- [star drone](star-drone.scd)

## SuperCollider Snippets

- [iter](iter.scd)
- [midi](midi.scd)
- [json-notation](json-notation.scd)
- [server](server.scd)

## Assorted Notes

### Channels & Buses

- Use Splay to mix an arbitrary number of channels into a stereo mix

SUBTLE NOTE:

Duplicate the UGen to make two identical UGens on server:

	x = {pinkNoise.ar(0.5)!2}.play;

Duplicate the argument to make two unique UGens on server:

	x = {pinkNoise.ar(0.5!2)}.play;


Duplicating a function is different than duplicating a statement

e.g.:

	rrand(50, 1200)!4;  >> [ 161, 161, 161, 161 ]
	{rrand(50, 1200)}!4  >> [ 928, 1035, 1044, 433 ]


exprand is different than ExpRand for synth defs

- exprand uses the same value for each synth instance
- ExpRand creates a new value for each synth instance

### Envelopes


- append `t_` to an argument to make it a Trig Control
- - it sends a control rate impulse at the set value, useful for triggers

e.g.:

```supercollider
SynthDef(\hi_hat, {
    arg vel = 0.3, t_gate=0;
    var sig, env;
    sig = WhiteNoise.ar(vel)!2;
    env = EnvGen.kr(Env.perc(0.001, 2, 1, -6), t_gate);
    sig = sig * env;
    Out.ar(0, sig);
}).add;

~hi_hat = Synth.new(\hi_hat);

MIDIdef.noteOn(\hit, {
    arg vel;
    ~hi_hat.set(\vel, vel.linlin(1,127,0.01,0.3), \t_gate, 1,);
});
```

- you can specify an array of curves for each section of the envelope, using numerical values or recognized symbols

e.g.:

	Env([0, 1, 0.85, 0], [1, 1, 2], [\sine, 4, -4]).plot;

### SuperCollider Server

	Nodes -> [Synth, Group]

*Busses:* pass signals between synths by sending audio to bus, then declaring that bus as in input in another synth

*Order of Execution:* Ordering of nodes on server

- `s.meter` pulls up audio meter for inputs and outputs
- `s.plotTree` shows a graphical representation of nodes on the server

- find number of available buses: `s.options.numAudioBusChannels;`

- find number of default input and output buses

	s.options.numOutputBusChannels;
	s.options.numInputBusChannels;

- you can set these variables to change them, but you have to call `s.reboot` for the changes to go into effect

By default, buses 0-7 are reserved for hardware output, and buses 8-15 are reserved for hardware input. Buses 16-127 are therefore safe for internal signal routing.

- specifying buses with hardcoded values is bad practice
- - Use `Bus` object to let SC allocate buses (either control or audio rate)

```supercollider
~reverbBus = Bus.audio(s, 1);
~toneBus = Bus.control(s, 1);

~reverbBus.index; //returns the index of the bus
```

When you output a multichannel signal to a single bus, the outputs are simply assigned to consecutive ascending buses beginning w the single bus

- It is your responsibility to make sure that the number of channels on the signal matches the number of channels on the bus

- The `addAction` can be important to maintain an appropriate order of synths on the server
e.g.: Generators can use the default `\addToHead` action, and effects can use `\addToTail` to ensure that the effects always come after the generators

- If you keep your sources in a group and your effects in a group, you can just make sure the groups are in order and not worry about the order of each individual synth


### Midi

Check out this [video tutorial for more info.](https://www.youtube.com/watch?v=Oz4KYZ9KLc0)

First do:

	MIDIClient.init;

Connect all available devices:

	MIDIIn.connectAll;

- `MIDIfunc`
- `MIDIdef` (subclass of `MIDIfunc`)
- `MIDIdef` has more consistent syntax

```
MIDIdef.noteOn(\noteOnTest, {'key down'.postln});
               ^name of MIDIdef   ^function to be evaluated
```

Disable and enable MIDIdefs:

	MIDIdef(\noteOnTest).disable;
	MIDIdef(\noteOnTest).enable;

Destroy:

	MIDIdef(\noteOnTest).free;

Free all:

	MIDIdef.freeAll;

`permanent` determines whether or not the `MIDIdef` persists ater Command `.`

```supercollider
MIDIdef.noteOn(\noteOnTest).permanent_(true);
MIDIdef.noteOn(\noteOnTest).permanent_(false);

MIDIdef.noteOn(\noteOnTest, {
    arg vel, nn, chan, src;
    [vel, nn].postln;
});
```

### Iteration

Iterations operate on collections like arrays

`[].do{}` evaluates the function for each item in the collection and then returns its receiver

```supercollider
[1, 2, 4, 6].do{
    arg item, count;
    [count, item].postln;
}
```

`[].collect{}` functions like `do`, but operates on a copy of the original array

```supercollider
[1, 2, 4, 6].collect{
    arg item;
    item.squared;
}
```

